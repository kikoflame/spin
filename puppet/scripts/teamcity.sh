#!/bin/sh
#
# /etc/init.d/teamcity -  startup script for teamcity
# @todo Convert to ERB template
#
export TEAMCITY_DATA_PATH="/var/opt/teamcity/.BuildServer"

case $1 in
start)
 start-stop-daemon --start -c teamcity --exec /var/opt/teamcity/current/bin/teamcity-server.sh start
;;

stop)
 start-stop-daemon --start -c teamcity --exec /var/opt/teamcity/current/bin/teamcity-server.sh stop
;;

esac

exit 0