class setup_postgresql {

  class { 'postgresql::server':
    postgres_password          => 'TPSrep0rt!'
  }

  postgresql::server::db { 'teamcity':
    user     => 'teamcity',
    password => postgresql_password('teamcity', 'teamcity123456'),
  }
}
