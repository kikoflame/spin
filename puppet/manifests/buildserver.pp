group { 'puppet': ensure => 'present' }

stage { 'database-services': before => Stage['build-services'] }
stage { 'build-services':    before => Stage['artifact-services']}
stage { 'artifact-services': }

class build-server {
  class { setup_postgresql:, stage => "database-services" }
  class { teamcity:,         stage => "build-services" }
  class { nexus:,            stage => "artifact-services" }
}

import 'postgresql.pp'
import 'teamcity.pp'
import 'nexus.pp'

include build-server