class nexus {
  include nexus::install
  include nexus::plugin::install
  include nexus::service
}

class nexus::install {

  $appversion="2.5.1"
  $srcurl="http://download.sonatype.com/nexus/oss"
  $filename="nexus-$appversion-bundle.tar.gz"
  $username="nexus"
  $installdir="/var/opt/nexus"
  $appdir="$installdir/$appversion"
  $checkrun="$appdir/bin/nexus"

  exec { "wget-$name":
    command => "/usr/bin/wget --output-document=$installdir/$filename $srcurl/$filename",
    creates => "$installdir/$filename",
    user    => $username,
    require => File["$installdir"],
  }

  # extract and move files, only if already not there (creates determines this)
  exec { "tar-$name":
    command => "/bin/tar xfz $installdir/$filename -C $appdir",
    user    => $username,
    creates => $runAll,
    require => [ File["$appdir"], Exec["wget-$name"]],
  }

  # setup user:group on installation
  group { $username:
    ensure => present,
  }

  user { $username:
    membership => inclusive,
    groups     => ["$username"],
    comment    => "'Nexus service account created by Puppet'",
  }

  file {"$installdir":
    ensure  => directory,
    owner   => $username,
    group   => $username,
    recurse => true,
    require => [ User[$username], Group[$username] ],
  }

  file {"$appdir":
    ensure  => directory,
    owner   => $username,
    group   => $username,
    recurse => true,
    require => [ User[$username], Group[$username] ],
  }

  file {"current":
    path    => "$appdir",
    ensure  => link,
    owner   => $username,
    group   => $username,
    target  => "$installdir/current"
  }

  file {"nexus-init":
    path    => "/etc/init.d/nexus",
    ensure  => file,
    owner   => "root",
    group   => "root",
    mode    => "a+rx,u+w",
    source  => "$appdir/bin/nexus"
  }
}

class nexus::plugin::install {
  @package { [ "unzip" ]:
    ensure => installed
  }

  $pluginversion="1.0.0"
  $pluginname="nexus-ruby-plugin"
  $srcurl="http://repo1.maven.org/maven2/org/sonatype/nexus/plugins/$pluginname/$pluginversion"
  $filename="$pluginname-$pluginversion-bundle.zip"
  $username="nexus"
  $installdir="/var/opt/nexus"
  $plugindir="$installdir/latest/WEB-INF/plugin-repository"

  exec { "wget-$name":
    command => "/usr/bin/wget --output-document=$installdir/$filename $srcurl/$filename",
    creates => "$installdir/$filename",
    user    => $username,
  }

  # extract and move files, only if already not there (creates determines this)
  exec { "tar-$name":
    command => "/bin/tar xfz $installdir/$filename -C $plugindir",
    user    => $username,
    creates => "$plugindir/bin/$pluginname-$pluginversion/$pluginname-$pluginversion.jar",
    require => [ Exec["wget-$name"]],
  }
}

class nexus::service {
  service { "nexus":
    ensure     => running,
    hasstatus  => false,
    hasrestart => true,
    enable     => true,
    require    => Class['nexus::install'],
  }
}