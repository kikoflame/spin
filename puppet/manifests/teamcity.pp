class teamcity {
  include teamcity::install
  include teamcity::service
  include teamcity::db::setup
}

class teamcity::install {
  @package { [ "java-1.6.0-openjdk", "wget", "tar", "gzip" ]:
    ensure => installed
  }

  $app_version="8.0.4"
  $src_url="http://download.jetbrains.com/teamcity"
  $filename="TeamCity-$appversion.tar.gz"
  $username="ubuntu"
  $install_dir="/var/opt/teamcity"
  $data_path="/var/opt/teamcity/.BuildServer"
  $app_dir="$install_dir/$app_version"
  $checkrun="$app_dir/bin/runAll.sh"
  $puppet_staging_directory="/tmp/packer-puppet-masterless"


  exec { "wget-$name":
    command => "/usr/bin/wget --output-document=$install_dir/$filename $src_url/$filename",
    creates => "$install_dir/$filename",
    user    => $username,
    require => File["$install_dir"],
  }

  exec { "tar-$name":
    command => "/bin/tar xfz $install_dir/$filename -C $app_dir",
    user    => $username,
    creates => $checkrun,
    require => [ File["$app_dir"], Exec["wget-$name"]],
  }

  group { $username:
    ensure => present,
  }

  user { $username:
    membership => inclusive,
    groups     => ["$username"],
    comment    => "'Teamcity service account created by Puppet'",
  }

  file {"$install_dir":
    ensure  => directory,
    owner   => $username,
    group   => $username,
    recurse => true,
    require => [ User[$username], Group[$username] ],
  }

  file {"$app_dir":
    ensure  => directory,
    owner   => $username,
    group   => $username,
    recurse => true,
    require => [ User[$username], Group[$username] ],
  }

  file {"$data_path":
    ensure  => directory,
    owner   => $username,
    group   => $username,
    recurse => true,
    require => [ User[$username], Group[$username] ],
  }

  file {"$install_dir/current":
    ensure  => link,
    target  => "$app_dir"
  }

  file {"teamcity.sh":
    path    => "/etc/init.d/teamcity",
    ensure  => file,
    owner   => "root",
    group   => "root",
    mode    => "a+rx,u+w",
    source  => "$puppet_staging_directory/scripts/teamcity.sh"
  }
}

class teamcity::service {
  service { "teamcity":
    ensure     => running,
    hasstatus  => false,
    hasrestart => true,
    enable     => true,
    require    => Class['teamcity::install'],
  }
}

class teamcity::db::setup {
  require teamcity::install
  require teamcity::service

  $src_url  = "http://jdbc.postgresql.org/download"
  $filename = "postgresql-9.1-903.jdbc4.jar"

  exec { "wget-$name":
    command => "/usr/bin/wget $src_url/$filename",
    cwd     => "$teamcity::install::install_dir/.BuildServer/lib/jdbc/",
    creates => "$filename",
    user    => $username,
  }

  file {"database.properties":
    path    => "$teamcity::install::install_dir/.BuildServer/config/database.properties",
    ensure  => file,
    owner   => $username,
    group   => $username,
    source  => "$puppet_staging_directory/config/teamcity.database.properties"
  }
}