group { 'puppet': ensure => 'present' }

stage { 'database': before => Stage['rvm'] }
stage { 'rvm': }

class app-server {
  class { setup_postgresql:, stage => "database" }
  class { setup_rvm:,        stage => "rvm" }
}

import 'postgresql.pp'
import 'rvm.pp'

include app-server