#!/bin/bash
#
# A simple bootstrap script to install puppet and required modules
# @see https://github.com/hashicorp/puppet-bootstrap for other examples
set -e

install_puppet() {
	if which puppet > /dev/null ; then
		echo "Puppet is already installed"
	else
		DISTRIB_CODENAME=$(lsb_release --codename --short)
		DEB="http://apt.puppetlabs.com/puppetlabs-release-${DISTRIB_CODENAME}.deb"
		DEB_PATH=$(mktemp)

		wget --output-document=$DEB_PATH $DEB
		sudo dpkg -i $DEB_PATH

		sudo apt-get update > /dev/null
		sudo apt-get install --yes puppet rubygems > /dev/null
		sudo gem install --no-ri --no-rdoc rubygems-update
		sudo update_rubygems > /dev/null
	fi
}

install_puppet