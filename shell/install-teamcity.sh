#!/bin/bash
#
# A simple bootstrap script to install teamcity.
#
set -e

install_teamcity() {
    INSTALL_DIR="/var/opt/teamcity"
    FILENAME="TeamCity-${1}.tar.gz"
    LATEST="${INSTALL_DIR}/latest"
    SRC_URL="http://download.jetbrains.com/teamcity/${FILENAME}"

    if [ ! -d "${INSTALL_DIR}" ]; then
        mkdir ${INSTALL_DIR}
    fi

    if [ ! -d "${INSTALL_DIR}/$1" ]; then
        mkdir ${INSTALL_DIR}/$1
        wget ${SRC_URL}
        tar -xf ${FILENAME} -C ${INSTALL_DIR}/$1
        rm ${FILENAME}

        if [ -L "${LATEST}" ]; then
            rm "${LATEST}"
        fi
        ln -s "${INSTALL_DIR}/$1" "${LATEST}"
    else
        echo "Teamcity-$1 is already installed."
    fi
}

if [ -z "$1" ]; then
    echo "No argument provided. Make sure to pass in a Teamcity version number. Eg: 8.0.2"
else
    install_teamcity $1
fi
